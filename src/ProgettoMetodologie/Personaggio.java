package ProgettoMetodologie;

import java.awt.Canvas;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Personaggio extends Canvas{

	protected Carattere c;

	protected int stileGioco;
	// Variabile che viene generata attraverso gli attributi del carattere che
	// detemina
	// lo stile di gioco che può essere pacifico aggressivo o neutro

	protected int fitness;
	// Variabile che contiene il livello di fitness [0-100]

	protected int id;
	// variabile che identifica univocamente un personaggio
	// *Lo farei intero solo perché è più semplice da generare

	protected Personaggio[] conoscenti;
	/*
	 * Array contenente gli agenti conosciuti dai giocatori è un array in quanto la
	 * capienza massima dei consocenti è data in input, verrà passata tramite
	 * costruttore.
	 */
	protected Tupla[] messaggiConoscenti;
	
	protected int indiceConoscenti;
	
	protected abstract int calcoloFitness();
	// Calcola il fitness di un agente

	protected abstract int calcoloStileGioco();
	// Calcola la strategia

	// protected abstract void mutazione();
	// Modifica la riproduzione
	
	protected HashMap<Agente,Integer> aggiunti;
	
	protected ArrayList<Agente> attivi;
	
	protected int posX;
	
	protected int posY;
	
	protected int ampiezzaRaggio;
	
	protected int raggio;
	
	protected int centroX;
	
	protected int centroY;
	
	public Personaggio() {
		aggiunti = new HashMap<>();
	}
	public void ricevi(Tupla messaggio) {
		int indiceMittente = this.getIndex(messaggio.getMit());
		messaggiConoscenti[indiceMittente] = messaggio;
		
	}
	public int getIndex(Personaggio p) {
		for (int i = 0; i < indiceConoscenti;i++) {
			if (conoscenti[i] == p) return i;
		}
		return 0;
	}
	public void scalaPosArray(int i) {
		for (int index = i; index < conoscenti.length-1;index++) {
			if (conoscenti[index] == null) break;
			conoscenti[index] = conoscenti[index+1];
		}
		if (indiceConoscenti == conoscenti.length-1) conoscenti[conoscenti.length-1] = null;
		indiceConoscenti -= 1;
		
	}
	public void disegna(Graphics g,Arena arena) {
		/*
		 * La funzione disegna è propria di ogni personaggio ed ogni volta che viene
		 * chiamata disegna il personaggio alla sua posizione tramite il metodo drawOval(),
		 * inoltre se il suo indice conoscenti è minore della lunghezza dell'array di conoscenti
		 * allora richiama la funzione cerca vicini() che gli permette di acquisire dei conoscenti.
		 */
		centroX = posX + ampiezzaRaggio / 2 - 2;
		centroY = posY + ampiezzaRaggio / 2 - 2;
		g.drawString(Integer.toString(this.id), centroX, centroY);
		//g.drawString("Fitness: " + Integer.toString(this.fitness), centroX, centroY+10);
		g.drawOval(posX,posY,ampiezzaRaggio,ampiezzaRaggio);
		g.drawOval(centroX,centroY,5,5);
		if (indiceConoscenti < conoscenti.length) cercaVicini(arena);
//		
//		  System.out.print("Conoscenti di " + this.id + " : "); for (int i = 0; i <
//		  conoscenti.length; i++) { if (conoscenti[i] !=
//		  null)System.out.print(conoscenti[i].id + ", "); } 
//		  System.out.println();
		 		
	}
	private void cercaVicini(Arena arena) {
		/*
		 * Controlla se per ogni giocatore attivo (tranne se stesso) se le coordinate posX,posY
		 * sono nel suo raggio di azione.
		 * Per evitare che aggiunga sempre gli stessi è stato creato un dizionario nel quale
		 * vengono registrati i giocatori aggiunti fino a quel momento,(è stato scelto il dizionario come
		 * struttura dati in quanto è in grado di velocizzare la ricerca), nel caso in cui il giocatori 
		 * sia presente non viene aggiunto
		 */
		for (Agente attivo : arena.attivi) {
			if (attivo == this) continue;
			if (aggiunti.get(attivo) == null) {
			if (indiceConoscenti > conoscenti.length - 2) break;
			if (posX <= attivo.posX && attivo.posX <= posX + ampiezzaRaggio) {
				if (posY <= attivo.posY && attivo.posY <= posY + ampiezzaRaggio) {
					//if (aggiunti.get(attivo) == null) 
						aggiunti.put(attivo, 1);
						conoscenti[indiceConoscenti] = attivo;
						indiceConoscenti += 1;
					}
					/*
					 * else if (aggiunti.get(attivo) < 1) { aggiunti.replace(attivo,
					 * aggiunti.get(attivo)+1); conoscenti[indiceConoscenti] = attivo;
					 * indiceConoscenti += 1; }
					 */
				}
			}
		}
		
	}

}
