package ProgettoMetodologie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Carattere {
	private ArrayList<Integer> presaBene = new ArrayList<>();
	/*
	 * Lista che contiene l'insieme dei naturali che hanno un influenza positiva sul
	 * livello di fitness di un agente
	 */

	private ArrayList<Integer> presaMale = new ArrayList<>();
	/*
	 * Lista che contiene l'insieme dei naturali che hanno un influenza negativa sul
	 * livello di fitness di un agente
	 */

	public Carattere(Carattere c1, Carattere c2, int interiDaPrendere) {
		/*
		 * Questo costruttore sarà utile per implementare la funzione di CrossOver della
		 * classe Personaggio
		 */
		ArrayList<Integer> supportoBene = new ArrayList<>();
		ArrayList<Integer> supportoMale = new ArrayList<>();

		supportoBene.addAll(c1.getPresaBene());
		supportoBene.addAll(c2.getPresaBene());

		supportoMale.addAll(c1.getPresaMale());
		supportoMale.addAll(c2.getPresaMale());

		HashMap<Integer, Integer> occorrenzePresaBene = contaOccorrenze(supportoBene);
		HashMap<Integer, Integer> occorrenzePresaMale = contaOccorrenze(supportoMale);

		ArrayList<Integer> doppiBene = estraiDoppi(occorrenzePresaBene);
		ArrayList<Integer> doppiMale = estraiDoppi(occorrenzePresaMale);

		aggiungi(interiDaPrendere, doppiBene, presaBene);
		aggiungi(interiDaPrendere, doppiMale, presaMale);

		completa(supportoBene, presaBene);
		completa(supportoMale, presaMale);

	}

	private void completa(ArrayList<Integer> supporto, ArrayList<Integer> presa) {
		// Completa il carattere dell'individuo
		while (presa.size() < 5) {
			int interoRandom = randInt(0, 9);
			int estratto = supporto.get(interoRandom);
			if (!presaBene.contains(estratto) && !presaMale.contains(estratto))
				presa.add(estratto);
		}
	}

	private void aggiungi(int interiDaPrendere, ArrayList<Integer> doppi, ArrayList<Integer> presa) {
		// Aggiunge al carattere gli interi comuni ai due genitori
		int i = 0;
		while (presa.size() < interiDaPrendere && i < doppi.size()) {
			presa.add(doppi.get(i));
			i++;
		}
	}

	private ArrayList<Integer> estraiDoppi(HashMap<Integer, Integer> occorrenze) {
		// Estrae le occorrenze maggiori di 1 dalla HashMap e ritorna un ArrayList
		ArrayList<Integer> doppi = new ArrayList<>();
		for (int chiave : occorrenze.keySet()) {
			if (occorrenze.get(chiave) >= 1)
				doppi.add(chiave);
		}
		return doppi;
	}

	private HashMap<Integer, Integer> contaOccorrenze(ArrayList<Integer> supporto) {
		// Crea una HashMap per contare le occorrenze
		HashMap<Integer, Integer> occorrenze = new HashMap<>();
		for (int intero : supporto) {
			if (occorrenze.containsKey(intero)) {
				int valore = occorrenze.get(intero);
				occorrenze.replace(intero, valore + 1);
			} else
				occorrenze.put(intero, 0);
		}
		return occorrenze;
	}

	public Carattere() {

		int intero;
		while (presaBene.size() < 5) {
			// Itera fino a quando la lista non contiene 5 elementi
			intero = randInt(0, 9);
			if (!presaBene.contains(intero))
				presaBene.add(intero);
		}
		while (presaMale.size() < 5) {
			// Itera fino a quando la lista non contiene 5 elementi
			intero = randInt(0, 9);
			if (!presaMale.contains(intero) && !presaBene.contains(intero))
				presaMale.add(intero);
		}
	}

	private void stampa() {
		/*
		 * Esempio di output: PresaBene: [2,8,9,0,1] PresaMale: [3,5,7,6,4]
		 */
		System.out.println("PresaBene: " + presaBene);
		System.out.println("PresaMale: " + presaMale);
	}

	protected static int randInt(int min, int max) {
		// Genera un numero casuale compreso tra 2 valori
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public ArrayList<Integer> getPresaBene() {
		return this.presaBene;
	}

	public ArrayList<Integer> getPresaMale() {
		return this.presaMale;
	}
	public boolean inPresaBene(int valore) {
		if (presaBene.contains(valore)) return true;
		return false;
		
	}
}
