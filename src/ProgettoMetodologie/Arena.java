package ProgettoMetodologie;

import java.util.ArrayList;

public class Arena implements Cloneable{

	public  ArrayList<Agente> attivi;
	
	public  int nop; //NumberOfPlayer

	public  int valoreFitnessStandard;
	/*
	 * Valore standard a cui fare riferimento per confrontare con valore specifico
	 * di ogni singolo agente, indicato nelle specifiche del progetto... (valore
	 * ottimale - punto 3)
	 */
	public int turno;
	
	public int sogliaFigli;
	
	public int sogliaMorte;
	
	public Arena(int giocatoriAttivi, int numeroConoscenti, int sogliaFigli, int sogliaMorte) {
		attivi = new ArrayList<Agente>();
		rendiAttivi(giocatoriAttivi, numeroConoscenti);
		valoreFitnessStandard = Carattere.randInt(60, 90);
		this.sogliaFigli = sogliaFigli;
		this.sogliaMorte = sogliaMorte;
		nop = giocatoriAttivi;
	}

	private void rendiAttivi(int n, int numeroConoscenti) {
		// Questa funzione genere n agenti attivi e li inserisce nella lista
		for (int i = 0; i < n; i++) {
			attivi.add(new Agente(i, numeroConoscenti,attivi));
		}
	}

	public void selezioneNaturale() {
		/*
		 * Non so se è il posto giusto dove definirla... teoricamente dovrebbe
		 * analizzare i giocatori che hanno un livello di fitness basso e poi dovrebbe
		 * chiamare la funzione uccidi passandogli come parametro l'agente per
		 * eliminarlo dalla lista
		 */

		ArrayList<Agente> daEliminare = new ArrayList<Agente>();
		for (Agente a : attivi) {
			if (a.fitness < 0 || attivi.size() < 1 || a.indiceConoscenti < 3)daEliminare.add(a);
		}
		daEliminare.forEach( (Agente a) -> uccidi(a) );
	}

	private void uccidi(Agente a) {
		/*
		 * Dato un agente, lo elimina dalla lista
		 * e viene eliminato anche dagli array dei suoi amici
		 */
		System.out.println("sto uccidendo: " + a.id);
		for (int i = 1; i < a.indiceConoscenti;i++) {
			Personaggio amico = a.conoscenti[i];
			if (amico.getIndex(a) != 0) 
				amico.scalaPosArray(amico.getIndex(a));
		}
		attivi.remove(a);
	}
	
	public void controllaFitness() {
		ArrayList<Agente> daAggiungere = new ArrayList<Agente>();
		ArrayList<Agente> daEliminare = new ArrayList<Agente>();
		for (Agente attivo : attivi) {
			if (attivo.fitness >= sogliaFigli && attivi.size() < 100) {
				nop += 1;
				daAggiungere.add(Agente.riproduzione(nop,attivo,(Agente)attivo.conoscenti[0]));
			}
			if (attivo.fitness <= sogliaMorte || attivo.eta > 20000){
				daEliminare.add(attivo);
			}
		}
		daAggiungere.forEach( (Agente a) -> attivi.add(a) );
		daEliminare.forEach( (Agente a) -> uccidi(a) );
	}
	
	public void interazione() {
		/*
		 * All'inizio scrivono tutti un messaggio
		 * 
		 * send()
		 * Per ogni agente attivo, genera un intero che corrisponde al messaggio
		 * e crea una Tupla messaggio, la quale contiene sia il testo del messaggio 
		 * che il mittente.
		 */
		send();
		elabora();
		ask();
		elabora();
	}
	
	public void send() {
		for (Agente attivo : attivi) {
			int intero = attivo.generaMessaggio(attivo.fitness, attivo.stileGioco);
			Tupla messaggio = new Tupla(intero,attivo);
			for (int i = 1; i < attivo.indiceConoscenti - 1; i++) {
				attivo.conoscenti[i].ricevi(messaggio);
			}
		}
	}
	
	public static void stampaArray(Object[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("pos" + i + ": " +array[i]);
		}
	}
	
	public void elabora() {
		/*
		 * Permette di far eleborare le risposte ai giocatori
		 * quindi di modificare il loro livello di fitness
		 */
		for (Agente attivo : attivi) {
			int prima, dopo;
			prima = attivo.fitness;
			for (int i = 1; i < attivo.indiceConoscenti - 1; i++) {
				if (attivo.messaggiConoscenti[i] == null) continue;
				int messaggio = attivo.messaggiConoscenti[i].getMex();
				if (attivo.c.inPresaBene(messaggio)) {
					if (attivo.fitness < 90 ) 
						{attivo.fitness += 2 * attivo.c.getPresaBene().indexOf(messaggio);}//Messaggio positivo
				}
				else
					attivo.fitness -= 2 * attivo.c.getPresaMale().indexOf(messaggio);
			}
			dopo = attivo.fitness;
			if (prima != dopo) System.out.println("ID: " + attivo.id + " cambiato | " 
			+ "prima: " + prima + " dopo: " + attivo.fitness);
		}
	}
	public void ask() {
		for (Agente attivo : attivi) {
			int intero = attivo.generaMessaggio(attivo.fitness, attivo.stileGioco);
			Tupla messaggio = new Tupla(intero,attivo);
			for (int i = 1; i < attivo.indiceConoscenti-1; i++) {
				if (attivo.messaggiConoscenti[i] == null) continue;
				attivo.messaggiConoscenti[i].getMit().ricevi(messaggio);
			}
		}
	}
	
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		/*
		 * Questa funzione mi permette di restituire un clone dell'originale.
		 * Utile per la gestione della cronologia in quanto ogni volta viene creato
		 * un nuovo oggetto arena in quel momento con un certo numero di giocatori
		 * il quale verrà utilizzato nel momento che viene attivato il replay.
		 * Se non avessi utilizzato questa funzione non sarebbe stato possibile effettuare quest'azione.
		 */
		Arena nuova = (Arena) super.clone();
		nuova.attivi = (ArrayList<Agente>) attivi.clone();
		nuova.attivi = new ArrayList<Agente>();
		for (Agente attivo : attivi) {
			nuova.attivi.add((Agente) attivo.clone());
		}
		return nuova;
	}

}
