package ProgettoMetodologie;

public class Tupla {
	
	private int messaggio;
	private Personaggio mittente;
	
	public Tupla(int messaggio, Personaggio mittente) {
		this.messaggio = messaggio;
		this.mittente = mittente;
	}
	public int getMex() {
		return messaggio;
	}
	public Personaggio getMit() {
		return mittente;
	}

}
