package ProgettoMetodologie;

public interface Messaggio {
    /*
    interfaccia che permette agli Agenti di comunicare
     */

    int generaMessaggio(int fitness, int stileGioco);
    //genera messaggio in base al livello di fitness dell'agente
}
