package ProgettoMetodologie;


import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;


public class Start{
	/*
	 * Questa classe gestisce tutto il sistema
	 * Quando viene avviata crea un ogetto grafica a cui passa la sua arena
	 * ed entra in ciclo che itera affinche il numero dei giocatori non è uguale a 0
	 * e fino a quando la variabile interrompi non diventa vera.
	 * Ogni volta che itera richiama la funzione update dell'oggetto grafica
	 * il quale disegna i personaggi, ogni volta incrementa il turno di 1.
	 * 
	 * NOTA: Ho inserito per prova i metodi controllaNumeroConoscenti() e controllaFitness()
	 * per verificare se il sistema si evolve correttamente, ovviamente nell'implementazione finale
	 * sarà modificato.
	 */
	
	public static boolean stoppato = false;
	public static boolean interrompi = false;
	public static boolean indietro = false;
	public static boolean avanti = false;
	public static boolean startWin = true;
	public static long inizio;
	public static  Arena arena;
	public static int nGiocatori;
	public static int nConoscenti;
	public static int sFigli;
	public static int sMorte;
	
	public static void main(String[] args) throws InterruptedException, IOException, CloneNotSupportedException {
		
		
		
		/*
		 * for (Agente attivo : arena.attivi) { System.out.println("ID: " +
		 * attivo.getId()); System.out.println("Fitness: " + attivo.getFitness());
		 * System.out.print("Conoscenti: "); attivo.printConoscenti();
		 * System.out.println("Stile di gioco: " + attivo.calcoloStileGioco());
		 * System.out.println("Genera messaggio: " +
		 * attivo.generaMessaggio(attivo.fitness, attivo.calcoloStileGioco())); }
		 */
		finestraIniziale();
		System.out.println(nGiocatori);
		arena = new Arena(nGiocatori, nConoscenti,sFigli,sMorte);
		inizio = System.currentTimeMillis();
		Grafica g = new Grafica(arena);
		while (!interrompi) {
			Thread.currentThread();
			Thread.sleep(24);
			g.update(arena);
			arena.turno += 1;
			if (arena.turno % 200 == 0) arena.selezioneNaturale();
			if (arena.turno % 300 == 0) arena.interazione();
			if (arena.turno % 400 == 0) arena.controllaFitness();
			if (arena.attivi.size() == 0) {g.tuttiMorti = true;}
			//if (arena.turno % 200 == 0) arena.controllaNumeroConoscenti(); 
			//if (arena.turno % 300 == 0) arena.controllaFitness();
		}
		System.exit(0);
	}
	
	public static void finestraIniziale() {
		JFrame jf = new JFrame("Menu Iniziale");
		Container c = jf.getContentPane();
		JPanel panel = new JPanel(new MigLayout());
		panel.setBackground(Color.BLACK);
		
		JLabel numeroGiocatori = new JLabel("Numero dei giocatori");
		JTextField textNumeroGiocatori = new JTextField(10);
		
		JLabel numeroConoscenti = new JLabel("Numero dei conoscenti");
		JTextField textNumeroConoscenti = new JTextField(10);
		
		JLabel sogliaFigli = new JLabel("Soglia per figli");
		JTextField textSogliaFigli = new JTextField(10);
		
		JLabel sogliaMorte = new JLabel("Soglia per morte");
		JTextField textSogliaMorte = new JTextField(10);
		JButton invio = new JButton("invio");
	
		numeroGiocatori.setForeground(Color.WHITE);
		numeroConoscenti.setForeground(Color.WHITE);
		sogliaFigli.setForeground(Color.WHITE);
		sogliaMorte.setForeground(Color.WHITE);
		invio.setForeground(Color.BLACK);
		
		panel.add(numeroGiocatori);
		panel.add(textNumeroGiocatori , "wrap");
		panel.add(numeroConoscenti);
		panel.add(textNumeroConoscenti, "wrap");
		panel.add(sogliaFigli);
		panel.add(textSogliaFigli, "wrap");
		panel.add(sogliaMorte);
		panel.add(textSogliaMorte, "wrap");
		panel.add(invio, "span, grow");
		invio.addActionListener(new ActionListener() {@Override
		public void actionPerformed(ActionEvent e) { Start.startWin = false;} });
		c.add(panel);
		jf.setSize(240,180);
		jf.setVisible(true);
		jf.setResizable(false);
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		while (Start.startWin) {
			System.out.println(Start.startWin);//Se lo tolgo non si avvia, DA INDAGARE!!!
			if (!Start.startWin) {
				try {
					if (textNumeroGiocatori.getText() == "") 
						Start.startWin = true;
					else 
						Start.nGiocatori = Integer.decode(textNumeroGiocatori.getText());
					if (textNumeroConoscenti.getText() == "")
						Start.startWin = true;
					else
						Start.nConoscenti = Integer.decode(textNumeroConoscenti.getText());
					if (textSogliaFigli.getText() == "")
						Start.startWin = true;
					else
						Start.sFigli = Integer.decode(textSogliaFigli.getText());
					if (textSogliaMorte.getText() == "")
						Start.startWin = true;
					else
						Start.sMorte = Integer.decode(textSogliaMorte.getText());
				}
				catch (NullPointerException e) {
					Start.startWin = true;
				}
				catch (NumberFormatException e) {
					Start.startWin = true;
				}
			}
		}
		
	}
}


