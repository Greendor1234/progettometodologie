package ProgettoMetodologie;

import java.util.ArrayList;

public class Agente extends Personaggio implements Messaggio, Cloneable {
	
	private int codiceMovimento;
	
	public int eta; //Se l'agente ha più di mille turni muore

	public Agente(int id, int numeroConoscenti, ArrayList<Agente> attivi) {
		super();
		eta = 0;
		this.attivi = attivi;
		this.id = id; // Assegna un id
		c = new Carattere(); // Crea un nuovo carattere
		conoscenti = new Personaggio[numeroConoscenti]; // Inizializza array conoscenti
		messaggiConoscenti = new Tupla[numeroConoscenti];
		fitness = calcoloFitness();// Calcola il livello di fitness casualmente
		stileGioco = calcoloStileGioco();
		indiceConoscenti = 1;
		conoscenti[0] = this;
		messaggiConoscenti[0] = null;
		assegnaCordinate();
	}
	private void assegnaCordinate() {
		/*
		 * Questa funzione viene richiamata nel costruttore dell'agente e permette
		 * di assegnare delle coordinate ed un'ampiezzaRaggio casuali, in questo modo
		 * viene garantità la casualità del sistema
		 */
		posX = Carattere.randInt(0, 600);
		posY = Carattere.randInt(0, 400);
		ampiezzaRaggio = Carattere.randInt(0, 60);
		raggio = ampiezzaRaggio / 2;
		codiceMovimento = Carattere.randInt(0, 7);
	}
	
	public void aggiornaPos() {
		/*
		 * Questa funzione aggiorna la posizione dei personaggi
		 * Se il personaggio si avvicina ai bordi il suo codiceMovimento viene cambiato
		 * e dunque cambierà direzione. 
		 * Grazie a questa funzione infatti i personaggi rimangono nei bordi
		 */
		eta += 1;
		if (posX <= 0) {
			int[] latoSx = {5,7,0,2,1};
			int i = Carattere.randInt(0, 4); 
			codiceMovimento = latoSx[i]; 
		}
		if (posX >= 599 - ampiezzaRaggio) {
			int[] latoDx = {1,5,3,6,4};
			int i = Carattere.randInt(0,4);
			codiceMovimento = latoDx[i];
		}
		if (posY < 0) {
			 int[] latoTop = {0,4,1,2,3}; 
			 int i = Carattere.randInt(0, 4); 
			 codiceMovimento = latoTop[i];
			 
		}
		if (posY >= 399 - ampiezzaRaggio) {
			int[] latoDown = {0,4,5,6,7}; 
			int i = Carattere.randInt(0, 2);
			codiceMovimento = latoDown[i];
		}
		
		if (codiceMovimento == 0) posX += 1; //orizzontale
		else if (codiceMovimento == 1) posY += 1; //verticale
		else if (codiceMovimento == 2) {posX += 1;posY += 1;} //diagonale\
		else if (codiceMovimento == 3) {posX -= 1;posY += 1;} //diagonale /
		else if (codiceMovimento == 4) posX -= 1;
		else if (codiceMovimento == 5) posY -= 1;
		else if (codiceMovimento == 6) {posX -= 1; posY-= 1;}
		else if (codiceMovimento == 7) {posX += 1; posY -= 1;}
			
	}

	public Agente(int id, int numeroConoscenti, Carattere c) {
		/*
		 * Questo costruttore è utile per generare un nuovo agente passandogli un
		 * carattere già creato. Questa funzione è utile quando si vuole generare un
		 * Agente partendo dal carattere dei suoi genitori, infatti il carattere che gli
		 * viene passato è stato creato in base al carattere dei genitori
		 */
		super();
		eta = 0;
		this.id = id;
		conoscenti = new Personaggio[numeroConoscenti];
		messaggiConoscenti = new Tupla[numeroConoscenti];
		this.c = c;
		fitness = calcoloFitness();
		stileGioco = calcoloStileGioco();
		indiceConoscenti = 1;
		conoscenti[0] = this;
		messaggiConoscenti[0] = null;
	}

	public int getFitness() {
		return this.fitness;
	}

	@Override
	protected int calcoloFitness() {
		return Carattere.randInt(Start.sMorte + 5, Start.sFigli-5);
	}

	@Override
	public int calcoloStileGioco() {
		/*
		 * Più pari ci sono in presa bene e più il giocatore è pacifico Più dispari ci
		 * sono in presa male e più è aggressivo
		 * 
		 * Per Antonio... Ho fornito una versione alternativa, visto che sei te che ti
		 * occupi di questa funzione decidi te se eliminarla, modificarla o prendere
		 * spunto, qualsiasi cosa fai per me va bene...
		 * 
		 * Inoltre mettiamoci d'accordo sull'intervallo di interi
		 */
		int contaPari = 0;
		int contaDispari = 0;
		for (int intero : c.getPresaBene()) {
			if (intero % 2 == 0)
				contaPari += 1;
			else
				contaDispari += 1;
		}
		int percentualePari = (contaPari * 100) / 5;
		int percentualeDispari = (contaDispari * 100) / 5;

		if (percentualeDispari > percentualePari)
			return Carattere.randInt(3, 5);
		else
			return Carattere.randInt(0, 2);
	}

	public int getId() {
		return id;
	}

	public void printConoscenti() {
		for (int i = 0; i < conoscenti.length; i++) {
			System.out.print(conoscenti[i] + " ");
		}
		System.out.println();
	}

	public static Agente riproduzione(int id, Agente g1, Agente g2) {
		/* L'id sarà la grandezza della lista degli agenti attivi
		 * Ritorna un nuovo personaggio che ha caratteristiche simili ai genitori
		 *
		 * L'idea è quella di prendere una certa percentuale calcolata casualmente di
		 * carattere(quindi di interi di un genitore) e la restante parte dell'altro
		 * Il costruttore Agente prende in input: - la somma dell'id della madre con
		 * quello del padre (DA MODIFICARE!!!) - il numero di conoscenti standard di
		 * tutti i personaggi - ed un carattere il quale costruttore prende in input: -
		 * il carattere del padre - il carattere della madre - ed il numero di interi da
		 * prendere dalla lista di uno o dell'altro
		 */
		int percentuale = Carattere.randInt(30, 60);
		g1.c.getPresaBene();
		// n * percentuale / 100 = n interi da prendere in presaBene e presaMale
		int interiDaPrendere = ((g1.c.getPresaBene().size() * percentuale) / 100);
		Agente figlio = new Agente(id, g1.conoscenti.length, new Carattere(g1.c, g2.c, interiDaPrendere));
		ereditaCoordinate(figlio,g1,g2);
		return figlio;
	}
	
	private static void ereditaCoordinate(Agente figlio, Agente padre, Agente madre) {
		figlio.posX = padre.posX;
		figlio.posY = padre.posY;
		figlio.ampiezzaRaggio = madre.ampiezzaRaggio;
	}

	@Override
	public int generaMessaggio(int fitness, int stileGioco) {
		// metodo che genera messaggio in base al livello di fitness ed allo stile di
		// gioco

		int tipoMessaggio = (fitness + (stileGioco * 10));
		System.out.println("Fitness: " + fitness);
		System.out.println("StileGioco: " + stileGioco);
		System.out.println("TipoMessaggio: " + tipoMessaggio);
		int messaggio = 0;
		if (tipoMessaggio > 100) tipoMessaggio = tipoMessaggio - 100;
		if (tipoMessaggio > 20 && tipoMessaggio < 80)
			messaggio = Carattere.randInt(tipoMessaggio - 20, tipoMessaggio + 20);
		else if (tipoMessaggio < 20)
			messaggio = Carattere.randInt(0, tipoMessaggio + 20);
		else if (tipoMessaggio > 80)
			messaggio = Carattere.randInt(tipoMessaggio - 20, 100);

		// aggiungo un altro random per renderlo ancora più casuale???
		// es.
		/*
		 * int mexr= Carattere.randInt(0,100); messaggio = (messaggio+mexr)/2;
		 */

		return messaggio / 10;
	}


	@Override
	protected Object clone() throws CloneNotSupportedException {
		/*
		 * Questa funzione mi permette di restituire in output il clone di un agente.
		 * Quindi un clone(nuovo agente) che ha le stesse caratteristiche del originale.
		 */
		Agente clone = (Agente) super.clone();
		return clone;
		
	}
}

