package ProgettoMetodologie;

import java.util.ArrayList;

public class Stack<E>{
	
	/*
	 * Classica implementazione dello Stack
	 */
	
	private ArrayList<E> array;
	
	public Stack() {
		array = new ArrayList<E>();
	}
	
	public void push(E elemento) {
		if (array.size() > 200) array.remove(0);
		array.add(elemento);
	}
	public E pop(){
		if (array.size() == 0) return null;
		E elemento = array.get(array.size()-1);
		array.remove(array.size()-1);
		return elemento;
	}
	public void svuotaStack() {
		array.clear();
	}
	public boolean isEmpty() {
		if (array.size() == 0) return true;
		return false;
	}
}
