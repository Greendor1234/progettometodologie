package ProgettoMetodologie;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.ConcurrentModificationException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class Grafica extends JPanel implements KeyListener{
	
	public Disegno d;
	public Container contenitore;
	public JFrame jf;
	public Stack<Arena> tmpBack;
	public Stack<Arena> tmpForward;
	public Arena checkpoint;
	public boolean tuttiMorti;
	
	public Grafica(Arena arena) throws IOException, CloneNotSupportedException{
		tmpBack = new Stack<>();
		tmpForward = new Stack<>();
		jf = new JFrame("GameOfLife!");
		contenitore = jf.getContentPane();
		jf.addKeyListener(this);
		d = new Disegno(arena);
		tmpBack.push((Arena) arena.clone());
		contenitore.add(d);
		jf.setSize(600, 460);
		contenitore.setBackground(Color.BLACK);
		jf.setVisible(true);
		jf.setResizable(false);
		jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}
	
	public void update(Arena a) throws CloneNotSupportedException {
		/*
		 * Lo stack tmpBack ha grandezza massima di 200 elementi, quando la raggiunge viene
		 * eliminato il primo elemento inserito nello stack e viene inserito l'elemento che si vuole aggiungere
		 * in cima alla pila, in questo modo non si occupa eccessivamente la memoria.
		 */
		if (tuttiMorti) {Start.stoppato = true;d.pausa = true;d.tuttiMorti = true;d.repaint();}
		pausa();
		BufferStrategy bs = d.getBufferStrategy();
		if (bs == null) {
			d.createBufferStrategy(3);
		}
		tmpBack.push((Arena) a.clone());
		d.repaint();
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(KeyEvent e){
		// TODO Auto-generated method stub
		if (e.getExtendedKeyCode() == 32) {
			if (Start.stoppato) uscita();
			else {checkpoint = Start.arena;Start.stoppato = true;d.pausa = true;d.repaint();}
		}
		if (e.getExtendedKeyCode() == 69) {
			Start.interrompi = true;
		}
		if (e.getExtendedKeyCode() == 73) {
			Start.indietro = true;
		}
		if (e.getExtendedKeyCode() == 65) {
			Start.avanti = true;
		}
		//System.out.println(e.getExtendedKeyCode());
		// i == 73
		// a == 65
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}
	
	public void uscita() {
		/*
		 * Utilizzata per uscire dalla pausa
		 */
		tmpBack.svuotaStack();
		tmpForward.svuotaStack(); 
		d.arena = checkpoint;
		d.indietro = false;
		d.pausa = false;
		checkpoint = null;
		Start.stoppato = false;
		Start.indietro = false;
		Start.avanti = false;
	}
	public void pausa() {
		/*
		 * Questa funzione permette di gestire la modalità di pausa del gioco
		 * ovviamente il keyListener è sempre in ascolto e se interrompi diventa vero
		 * il programma esce.
		 * 
		 * Se indietro diventa vero, imposta la variabile indietro dell'arena a true
		 * ed estrare dallo stack temporaneo un arena, che inserisce sia nello stack temporaneo
		 * tmpForward (che permette di andare avanti), sia nell'arena della variabile d, in questo
		 * modo d è in grado di disegnare i giocatori delle arene passate
		 * 
		 * Se lo stack è vuoto semplicemente si ritorna al momento in cui è stato premuto backspace
		 * infatti quell'arena è memorizzata nella variabile checkpoint che è di tipo arena e quando si esce
		 * dalla pausa checkpoint viene reimpostato a null
		 * 
		 * Se avanti diventa vero, viene estratta ogni volta un'arena dallo stack ed inserita sia nello stack tmpBack
		 * sia nell'arena di d, infatti il procedimento è lo stesso.
		 */
		while(Start.stoppato) {
			getKeyListeners();
			if (Start.interrompi) System.exit(0);
			if (Start.indietro) {
				if (tmpBack.isEmpty()) {uscita();break;}
				d.indietro = true;
				Arena daStack = tmpBack.pop();
				tmpForward.push(daStack);
				d.arena = daStack;
				d.repaint();
				Start.indietro = false;
			}
			if (Start.avanti) {
				if (tmpForward.isEmpty()) {uscita();break;}
				Arena daStack = tmpForward.pop();
				tmpBack.push(daStack);
				d.arena = daStack;
				d.repaint();
				Start.avanti = false;
			}
		}		
	}
}

class Disegno extends Canvas {
	
	/*
	 * Classe Disegno che estende Canvas, in questa classe viene richiamato ogni volta
	 * il metodo paint(), (la chiamata viene effettuata dal metodo update() della classe Grafica)
	 * ogni volta che viene chiamato il metodo paint() controlla se la variabile indietro è vera o falsa
	 * infatti se è falsa continua ad aggiornare le posizioni dei personaggi, altrimenti disegna solamente
	 * lo stato dell'arena.
	 * Infatti contiene una variabile di tipo Arena che gli permette di accedere ai giocatori attivi, in quell'arena
	 */
	
	public Graphics grafica;
	public boolean indietro = false;
	public boolean pausa = false;
	public boolean tuttiMorti = false;
	public Arena arena;
	
	public Disegno(Arena arena){
		this.arena = arena;
		
	}
	
	@Override
	public void paint(Graphics g) {
		long tempo = System.currentTimeMillis() - Start.inizio;
		if (!indietro) {for (Agente a : arena.attivi) {	a.aggiornaPos();}}
		//else { for (Agente a : arena.attivi) {a.vecchiePos();} indietro = false;}
		g.setColor(Color.WHITE);
		g.drawString("Esc - e", 530,430);
		if (pausa) {
			if (tuttiMorti) {
				g.drawString("Fine", 250, 200);
				g.drawString("Tutti i giocatori sono morti", 200, 250);
				g.drawString("Turno: " + arena.turno/100, 0, 430);
				//g.drawString("indietro - i", 80, 430);
				
			}
			else {
				g.drawString("Turno: " + arena.turno/100, 0, 430);
				g.drawString("indietro - i", 80, 430);
				g.drawString("Riprendi - backspace", 170, 430);
				g.drawString("avanti - a", 325, 430);
			}
		}
		else {
			g.drawString("Turno: " + arena.turno/100, 0, 430);
			g.drawString("Attivi: " + arena.attivi.size(), 80, 430);
			g.drawString("Giocatori Esistiti: " + arena.nop, 160, 430);
			g.drawString("Pausa - backspace", 310, 430);
			g.drawString("Tempo: " + tempo  / 100 + "s" , 435, 430);
		}
		try {
			for (Agente a : arena.attivi) {	a.disegna(g,arena);}
		}
		catch (ConcurrentModificationException e) {
			
		}
		
	}

}